import webvtt
vtt = webvtt.read('subtitle.vtt')
transcript = ""

lines = []
tc = [] #timecode
for line in vtt:
    # Strip the newlines from the end of the text.
    # Split the string if it has a newline in the middle
    # Add the lines to an array
    toadd=line.text.strip().splitlines()
    lines.extend(toadd)
    for i in range(len(toadd)):
        tc.append(line.start_in_seconds)

# Remove repeated lines
previous = None
current=0
#print(len(tc))
#print(len(lines))
for line in lines:
    #print("current="+str(current))
    if line == previous:
       current+=1
       continue
    transcript += "  # " + \
        str(int(int(tc[current])/3600)) + "h" +\
        str(int(int(tc[current])/60)%60) + "m" +\
        str((int(tc[current]))%60) + "s" +\
        "\n" + line
    previous = line
    current+=1

print(transcript)
